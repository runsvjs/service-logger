'use strict';

class Log  {
	constructor(max){
		this.pointer = 0;
		this.maxLen = max;
		this.arr = new Array(max);
	}

	push(item){
		this.arr[this.pointer] = item;

		this.pointer++;

		if(this.pointer === this.maxLen){
			this.pointer = 0;
		}
	} 

	read(){
		const ret = [];
		const {arr, pointer, maxLen} = this;

		for(let i = pointer;i< maxLen;i++){
			const item = arr[i];
			if(item === undefined){
				break;
			}
			ret.push(item);
		}
		for(let i = 0;i<pointer;i++){
			const item = arr[i];
			if(item === undefined){
				break;
			}
			ret.push(item);
		}
		return ret;

	}
}
module.exports = Log;
