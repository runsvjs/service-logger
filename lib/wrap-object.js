'use strict';
const LogEntry = require('./log-entry');
function appendLevel(prev, next) {
	if (!prev) {
		return next;
	}
	return `${prev}.${next}`;
}

function measureCallback(fn, logEntry) {
	return function (...args) {
		logEntry.took = Date.now() - logEntry.start;
		return fn(...args);
	};
}
function measurePromise(fn, logEntry) {
	return async function (...args) {
		const ret = await fn(...args);
		logEntry.took = Date.now() - logEntry.start;
		return ret;
	};
}
function wrapObject(obj, log, level = '') {

	const handler = {
		get(target, prop, receiver) {
			const impl = Reflect.get(target, prop, receiver);

			if (typeof target[prop] === 'function') {
				return function proxyHandlerImpl(...args) {
					let lastArg = args.pop();
					const name = appendLevel(level, prop);
					let entry;

					if (typeof lastArg === 'function') {
						// lastArg is a callback. Do not push it into the logs.
						entry = new LogEntry(name, args);
						log.push(entry);
					} else {
						entry = new LogEntry(name, [...args, lastArg]);
						log.push(entry);
					}

					entry.start = Date.now();

					if (typeof lastArg === 'function') {
						return impl(...args, measureCallback(lastArg, entry));
					}
					if (impl.constructor.name === 'AsyncFunction' || impl.constructor.name === 'Promise') {
						return measurePromise(impl, entry)(...args, lastArg);
					}
					entry.took = -1;

					return impl(...args, lastArg);
				};
			}

			if (target[prop] == null) {
				return impl;
			}

			switch (target[prop].constructor.name) {
			case 'String':
			case 'Number':
			case 'Date':
			case 'Map':
			case 'Set':
			case 'Boolean':
			case 'Symbol':
				return impl;
			}
			return wrapObject(impl, log, appendLevel(level, prop));

		},
	};

	const proxy = new Proxy(obj, handler);
	return proxy;
}
module.exports = {
	wrapObject,
	appendLevel,
	measurePromise,
	measureCallback
};
