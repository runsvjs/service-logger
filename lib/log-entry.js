'use strict';

class LogEntry {
	constructor(fn, args){
		this.fn = fn;
		this.args = args;
		this.start = null;
		this.took = null;
	}
}
module.exports = LogEntry;
