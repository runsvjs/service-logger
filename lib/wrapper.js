'use strict';

const Log = require('./log');
const {wrapObject} = require('./wrap-object');

class ServiceLogger {}

const defaults = {
	queueLen: 10
};

function wrap(service, options = defaults) {
	const {queueLen} = {...defaults, ...options};
	const log = new Log(queueLen);

	const name = service.name;

	function F() {}

	F.prototype = new ServiceLogger();

	let ret = new F();
	ret.name = name;
	ret.start = function startImpl(...args) {
		return service.start(...args);
	};
	ret.stop = function stopImpl(...args) {
		return service.stop(...args);
	};
	ret.logs = function logsImpl() {
		return log.read();
	};

	if (!service.getClient) {
		return ret;
	}

	ret.getClient = function getClientLogged() {
		const client = service.getClient();
		return wrapObject(client, log);
	};

	return ret;
}
module.exports = wrap;
