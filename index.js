'use strict';

const ServiceLogger = require('./lib/service-logger');
const wrapper = require('./lib/wrapper');

function create(service, options) {
	options  = options || {};

	if(options.noWrap){
		return service;
	}
	delete options.noWrap;
	
	return wrapper(service, options);
}
module.exports ={
	ServiceLogger,
	create
};
