'use strict';

const assert = require('assert');
const wrapper = require('../lib/wrapper');
const runsv = require('runsv');

function wait(delay) {
	return new Promise(function (resolve) {
		setTimeout(resolve, delay);
	});
}

const primitives = {
	number: 123,
	date: new Date(Date.UTC(2024, 0, 6, 11, 59, 59)),
	str: 'string',
	symbol: Symbol(),
	map: new Map(),
	set: new Set()
};
function create(options) {
	const {name} = options;
	let client;
	return {
		name,
		start(deps, callback) {
			client = {
				serviceCall(...args) {
					const callback = args.pop();
					setTimeout(callback, 100, null, deps, args);
				},
				nested: {
					async nestedCall(...args) {
						await wait(100);
						return args;
					}
				},
				...primitives
			};
			setImmediate(callback);
		},
		stop(callback) {
			setImmediate(callback);
		},
		getClient: () => client
	};
}
function dep1() {
	return {
		name: 'dep1',
		start(_, callback) {
			callback();
		},
		stop(callback) {
			callback();
		},
		getClient() {
			return {name: 'dep1'};
		}
	};
}

describe('#wrapper(service, options)', function () {

	before(function (done) {
		const sv = runsv.create();
		const myService = wrapper(create({name: 'n1'}));
		this.myService = myService;

		const dep = dep1();
		sv.addService(dep);
		sv.addService(myService, dep);
		this.sv = sv;
		this.sv.start(function (err) {
			done(err);
		});
	});
	before(function actCallCallbackService(done) {
		const self = this;
		const {sv} = self;
		const {n1} = sv.getClients();

		n1.serviceCall('a', 'b', function (...args) {
			self.callbackArgs = args;
			done();
		});
	});
	before(async function actCallPromiseService() {
		const self = this;
		const {sv} = self;
		const {n1} = sv.getClients();

		const ret = await n1.nested.nestedCall('c');
		self.nestedCallbackArgs = ret;
	});
	after(function (done) {
		this.sv.stop(done);
	});
	it('wrapped service should work as expected', function () {
		const {callbackArgs} = this;
		const [err, deps, args] = callbackArgs;
		assert(err == null);
		assert.deepStrictEqual(deps, {dep1: {name: 'dep1'}});
		assert.deepStrictEqual(args, ['a', 'b']);
	});
	it('calls should be logged', function () {
		const {myService} = this;

		const actual = myService.logs().map(({fn, args}) => ({fn, args}));
		const expected = [
			{fn: 'serviceCall', args: ['a', 'b']},
			{fn: 'nested.nestedCall', args: ['c']},
		];

		assert.deepEqual(actual, expected);
	});
	it('logs should include start time (and time taken)', function () {
		const {myService} = this;

		const expected = [
			{fn: 'serviceCall', start: Date.now(), took: 100},
			{fn: 'nested.nestedCall', start: Date.now(), took: 100}
		];
		const actual = myService.logs().map(({fn, start, took}) => ({fn, start, took}));

		const error = 20;
		let i = 0;
		for (; i < expected.length; i++) {
			const a = expected[i];
			const b = actual[i];
			const expectedDiffStart = error +
				expected.map(x => x.took).reduce((total, curr) => ((total || 0) + curr));
			assert(Math.abs(a.took - b.took) < error);
			assert(Math.abs(a.start - b.start) < expectedDiffStart, a.start - b.start);
		}
		assert(i == 2, 'no test performed');
	});
	it('should be able to access primitives', function () {
		const {n1} = this.sv.getClients();
		for (let [key, expected] of Object.entries(primitives)) {
			const actual = n1[key];
			assert.equal(actual, expected);
		}
	});
});
