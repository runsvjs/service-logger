'use strict';

const assert = require('assert');
const Log = require('../lib/log');

describe('Log', function(){
	it('should keep last N elements', function(){

		const tests = [
			[5, 3, [0, 1, 2]],
			[5, 10, [5, 6, 7, 8, 9]],
		];

		const results = [];

		for(let [maxLen, writeItems] of tests){
			let log = new Log(maxLen);
			for(let i = 0; i< writeItems; i++){
				log.push(i);
			}
			results.push([ maxLen, writeItems, log.read()]);
		}

		for(let i = 0; i< results.length;i++){
			const actual = results[i][2];
			const expected = tests[i][2];
			assert.deepStrictEqual(actual, expected);
		}
	});
});
